#sm_resultAnalysis.py
"""
 SM Result Analysis
 Author:  Vinod K
          SM RED Labs
 Contact: info@smredlabs.com
"""
import time
import sys
import os
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from cStringIO import StringIO
import re
import xlwt
from colorama import Fore, Back, Style, init
init()
def analyzer():
 filePath='pdf/'
 if not os.path.exists(filePath):
	print(Fore.RED + "PDF folder Not Found !")
	sys.exit();
 start_time = time.time()
 print(Back.GREEN)
 print(Fore.RED + 'Calicut University B.Tech Result Analyzer')
 print(Fore.RESET + Back.RESET + Style.RESET_ALL)
# Converting PDF to text using PDFMiner
 def convert_pdf_to_txt(path):
    rsrcmgr = PDFResourceManager()
    retstr = StringIO()
    codec = 'utf-8'
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    fp = file(path, 'rb')
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    password = ""
    maxpages = 0
    caching = True
    pagenos = set()

    for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password, caching=caching,
                                  check_extractable=True):
        interpreter.process_page(page)

    text = retstr.getvalue()

    fp.close()
    device.close()
    retstr.close()
    return text

 # Defining various styles for the excel sheet
 style0 = xlwt.easyxf('font: name Times New Roman, bold on; align: wrap on, horiz center; borders: left thin, right thin, top thin, bottom thin;')
 styleLeftNorm = xlwt.easyxf('font: name Times New Roman; align: wrap on, horiz left; borders: left thin, right thin, top thin, bottom thin;')
 styleCenterNorm = xlwt.easyxf('font: name Times New Roman; align: wrap on, horiz center; borders: left thin, right thin, top thin, bottom thin;')
 styleLeftBold = xlwt.easyxf('font: name Times New Roman, bold on; align: wrap off, horiz left; borders: left thin, right thin, top thin, bottom  thin;')
 i = 1
 n = 120
 row = 3
 noOfStudents = 0
 while i <= n:
    try:
        firstText = convert_pdf_to_txt(filePath + str(i) + '.pdf')
        break
    except IOError:
        i += 1
        continue

 i = 1
 subCount = 0
 subList = []
 collegeMatch = re.search(r'CENTER[A-Z a-z.,&:]*', firstText)
 if collegeMatch:
    college = collegeMatch.group()
    college = college[11:].strip()

 semMatch = re.search(r':[IV and]+', firstText)
 if semMatch:
    sem = semMatch.group()
    sem = sem[1:].strip()

 branchMatch = re.search(r':[A-Za-z. &]*ENGINEERING', firstText) #matching end of string
 if branchMatch:
    branch = branchMatch.group()
    branch = branch[1:].strip()

 subNameStarts = False
 checkSubMatch = True
 subNamesList = []
 compareNewLineMatch = False
 subCodeLineIndex = {}
 lineTemp = 0
 bufferSubName = ""
 for text in firstText.splitlines():
    subMatch = re.search(r'^[A-Z][A-Z][0-9 (P)(p)]+', text)
    subEntryMatch = re.search(r'Credits', text)
    monthYearMatch = re.search(r'^[A-Z]+,\d\d\d\d', text)
    newLineMatch = re.search(r'$', text) 
    if newLineMatch and compareNewLineMatch:
        if subList[subCount-1] in subCodeLineIndex:
            subCodeLineIndex[subList[subCount-1]] += 1
        else:
            subCodeLineIndex[subList[subCount-1]] = 0

        if subCodeLineIndex[subList[subCount-1]] >= 3:
            subCodeLineIndex[subList[subCount-1]] = 0
    if subMatch and checkSubMatch:	
        subList.append(subMatch.group())
        subCount += 1
        compareNewLineMatch = True
    elif subEntryMatch:
	checkSubMatch = False
        subNameStarts = True
        compareNewLineMatch = False
        continue
    elif monthYearMatch:
        monthYear = monthYearMatch.group()

    if subNameStarts:
        subStopMatch = re.search(r'\d', text)
        if subStopMatch:
            subNameStarts = False
            continue
        subNameMatch = re.search(r'[^&][A-Z &]*', text)
        if subNameMatch:
            subName = subNameMatch.group()
            # if subName != "ENGINEERING" and subName[0] != " ":
            try:	
            	if subCodeLineIndex[subList[lineTemp]] == 0:
                	subNamesList.append(bufferSubName + " " + subName)
                	bufferSubName = ""
                	# print(subNameMatch.group())
                	lineTemp += 1
            	else:
                	subCodeLineIndex[subList[lineTemp]] = 0
                	bufferSubName = subName
	    except:
		continue

 wb = xlwt.Workbook()
 ws = wb.add_sheet('Results')
 # sheet.merge(top_row, bottom_row, left_column, right_column)
 ws.write_merge(0, 0, 0, subCount+4, college,style0)
 ws.write_merge(1, 1, 0, subCount+4, sem + ' Semester B.Tech Results '+ monthYear,style0)
 ws.write_merge(2, 2, 0, subCount+4, 'Branch : '+ branch,style0)
 ws.write(3, 0, "Reg.No.",style0)
 ws.write(3, 1, "Name",style0)
 col = 2
 subGrades = {}
 for sub in subList:
    ws.write(3, col, sub,style0)
    subGrades[(col, 'S')] = 0
    subGrades[(col, 'A')] = 0
    subGrades[(col, 'B')] = 0
    subGrades[(col, 'C')] = 0
    subGrades[(col, 'D')] = 0
    subGrades[(col, 'E')] = 0
    subGrades[(col, 'U')] = 0
    col += 1
 ws.write(3, col, "SGPA",style0)
 ws.write(3, col+1, "No.of Arrears",style0)
 ws.write(3, col+2, "Arrears Subjects",style0)
 totalPass = 0
 rankGPA = {'first': 0, 'second': 0, 'third': 0}
 rankName = {'first': '', 'second': '', 'third': ''}
 while i <= n:
    try:
        resultString = convert_pdf_to_txt(filePath + str(i) + '.pdf')
        noOfStudents += 1
    except IOError:
        i += 1
        continue
    tgrade = ""
    column = 2
    row += 1
    noOfArrears = 0
    arrears = ""
    for line in resultString.splitlines():
        regnoMatch = re.search(r':[A-Z ]+\d\d\d$', line)
        nameMatch = re.search(r':[A-Z .]+$', line)
        gradeMatch = re.search(r'(^[A-FSU]$|^U\(A)', line)
        gpaMatch = re.search(r'\d\.\d\d([*]?)', line)

        if gpaMatch is None:
            gpaMatch = re.search(r':[ ]*\d(\.\d)?([*]?)', line)
        if nameMatch:
            name = nameMatch.group()
            name = name[1:].strip()
            if len(name) > 3:
                ws.write(row, 1, name, styleLeftBold)

        elif regnoMatch:
            regno = regnoMatch.group()
            regno = regno[1:]
            ws.write(row, 0, regno, styleLeftBold)

        elif gradeMatch:
            subNameStarts = False
            grade = gradeMatch.group();
            if grade == "U(A":
                grade = "U"
            if grade == "U":
                noOfArrears += 1
                subGrades[(column, 'U')] += 1
                arrears = arrears + str(column-1) + ', '
            if grade == "S":
                subGrades[(column, 'S')] += 1
            if grade == "A":
                subGrades[(column, 'A')] += 1
            if grade == "B":
                subGrades[(column, 'B')] += 1
            if grade == "C":
                subGrades[(column, 'C')] += 1
            if grade == "D":
                subGrades[(column, 'D')] += 1
            if grade == "E":
                subGrades[(column, 'E')] += 1
            ws.write(row, column, grade, styleCenterNorm)
            column += 1
            tgrade = tgrade + grade + ","
        elif gpaMatch:
            gpa = gpaMatch.group()
            if gpa.startswith(':'):
                gpa = gpa[1:].strip()

            if gpa.endswith('*'):
                pass
            else:
                if rankGPA['first'] < float(gpa):
                    rankGPA['third'] = rankGPA['second']
                    rankName['third'] = rankName['second']
                    rankGPA['second'] = rankGPA['first']
                    rankName['second'] = rankName['first']
                    rankGPA['first'] = float(gpa)
                    rankName['first'] = name
                elif rankGPA['second'] < float(gpa):
                    rankGPA['third'] = rankGPA['second']
                    rankName['third'] = rankName['second']
                    rankGPA['second'] = float(gpa)
                    rankName['second'] = name
                elif rankGPA['third'] < float(gpa):
                    rankGPA['third'] = float(gpa)
                    rankName['third'] = name
            ws.write(row, column, gpa, styleCenterNorm)

        breakMatch = re.search(r'LETTER GRADE', line)
        if breakMatch:
            break
    ws.write(row, col+1, noOfArrears, styleCenterNorm)
    if arrears != "":
        arrears = arrears[:-2]
    else:
        totalPass += 1
    ws.write(row, col+2, arrears, styleCenterNorm)
    #print("%s,%s,%s%s" % (regno, name, tgrade, gpa))
    print "Analyzing..."
    i += 1
 ws.write_merge(row+1, row+1, 0, 1, "Pass Percentage", styleLeftBold)
 ws.write_merge(row+2, row+2, 0, 1, "No of S grades", styleLeftBold)
 ws.write_merge(row+3, row+3, 0, 1, "No of A grades", styleLeftBold)
 ws.write_merge(row+4, row+4, 0, 1, "No of B grades", styleLeftBold)
 ws.write_merge(row+5, row+5, 0, 1, "No of C grades", styleLeftBold)
 ws.write_merge(row+6, row+6, 0, 1, "No of D grades", styleLeftBold)
 ws.write_merge(row+7, row+7, 0, 1, "No of E grades", styleLeftBold)
 ws.write_merge(row+8, row+8, 0, 1, "No of U grades", styleLeftBold)
 ws.write_merge(row+9, row+9, 0, 1, "No of Students Passed", styleLeftBold)

 col = 2
 for sub in subList:
    ws.write(row+1, col, round((((noOfStudents-subGrades[(col, 'U')]) / float(noOfStudents)) * 100), 2), styleCenterNorm)
    ws.write(row+2, col, subGrades[(col, 'S')], styleCenterNorm)
    ws.write(row+3, col, subGrades[(col, 'A')], styleCenterNorm)
    ws.write(row+4, col, subGrades[(col, 'B')], styleCenterNorm)
    ws.write(row+5, col, subGrades[(col, 'C')], styleCenterNorm)
    ws.write(row+6, col, subGrades[(col, 'D')], styleCenterNorm)
    ws.write(row+7, col, subGrades[(col, 'E')], styleCenterNorm)
    ws.write(row+8, col, subGrades[(col, 'U')], styleCenterNorm)
    ws.write(row+9, col, noOfStudents-subGrades[(col, 'U')], styleCenterNorm)
    col += 1

 ws.write_merge(row+10, row+10, 0, 1, "Total No of Students", style0)
 ws.write(row+10, 2, noOfStudents, styleCenterNorm)
 ws.write_merge(row+10, row+10, 3, 6, "Total Passed Students", style0)
 ws.write(row+10, 7, totalPass, styleCenterNorm)
 ws.write_merge(row+10, row+10, 8, 11, "Pass Percentage", style0)
 ws.write(row+10, 12, round((totalPass/float(noOfStudents))*100, 2), styleCenterNorm)

 ws.write_merge(row+11, row+11, 0, 1, "Class Topper", style0)
 ws.write(row+11, 2, '1', style0)
 ws.write_merge(row+11, row+11, 3, 6, rankName['first'], styleLeftBold)
 ws.write(row+11, 7, rankGPA['first'], style0)

 ws.write(row+12, 2, '2', style0)
 ws.write_merge(row+12, row+12, 3, 6, rankName['second'], styleLeftBold)
 ws.write(row+12, 7, rankGPA['second'], style0)

 ws.write(row+13, 2, '3', style0)
 ws.write_merge(row+13, row+13, 3, 6, rankName['third'], styleLeftBold)
 ws.write(row+13, 7, rankGPA['third'], style0)

 ws.write(row+15, 0, 'Sub Code', style0)
 ws.write_merge(row+15, row+15, 1, 4, "Sub Name", style0)
 ws.write_merge(row+15, row+15, 5, 7, "Staff Name", style0)
 ws.write(row+15, 8, 'Students Passed', style0)
 ws.write(row+15, 9, 'Pass %', style0)
 ws.write(row+15, 10, 'Grade S', style0)
 ws.write(row+15, 11, 'Grade A', style0)
 ws.write(row+15, 12, 'Grade B', style0)
 ws.write(row+15, 13, 'Grade C', style0)
 ws.write(row+15, 14, 'Grade D', style0)
 ws.write(row+15, 15, 'Grade E', style0)
 ws.write(row+15, 16, 'Grade U', style0)

 col = 2
 row = row +16
 subNameIndex = 0
 for sub in subList:
    ws.write(row, 0, sub, styleLeftBold)
    ws.write_merge(row, row, 1, 4, subNamesList[subNameIndex], styleLeftBold)
    ws.write_merge(row, row, 5, 7, "", styleLeftBold)
    ws.write(row, 8, noOfStudents-subGrades[(col, 'U')], style0)
    ws.write(row, 9, round((((noOfStudents-subGrades[(col, 'U')]) / float(noOfStudents)) * 100), 2), style0)
    ws.write(row, 10, subGrades[(col, 'S')], style0)
    ws.write(row, 11, subGrades[(col, 'A')], style0)
    ws.write(row, 12, subGrades[(col, 'B')], style0)
    ws.write(row, 13, subGrades[(col, 'C')], style0)
    ws.write(row, 14, subGrades[(col, 'D')], style0)
    ws.write(row, 15, subGrades[(col, 'E')], style0)
    ws.write(row, 16, subGrades[(col, 'U')], style0)
    col += 1
    row += 1
    subNameIndex += 1
 #fname=sem+branch+'results'+'.'+'xls'
 wb.save('results.xls')
 print(Back.GREEN)
 print(Fore.RED + 'Analysis report generated and saved as an excel file results.xls')
  # print("--- %s seconds ---" % (time.time() - start_time))
