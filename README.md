# README #
## Calicut University B.Tech Result Downloader and Analyzer ##

*This project is an amalgamation of [cu_pdf_downloader](https://bitbucket.org/shaiju1983/cu_pdf_downloader) and [SM Result Analysis](https://bitbucket.org/smredlabs/sm-result-analysis) projects*
### No Need to Install Python or other dependencies. Simply Run Result_DNA from the terminal/cmd.Supports 64bit Linux OS and 64bit Windows OS ###
*resultDNA_linx64     - Linux OS* 

*resultDNA_winx64.exe - Windows OS* 

**Download result_dna from [Downloads](https://bitbucket.org/shaiju1983/cu_result_dna/downloads) of Bitbucket.**

**Next Run result_dna as shown below:**

*Linux*
```
chmod +x resultDNA_linx64

./resultDNA_linx64
```
*Windows*
```
Double CLick on the 'resultDNA_winx64.exe' file
```
**If successful, a folder named "pdf" will be created(If the choice was 1).**

**If successful, an excel file named "results.xls" will be generated(If the choice was 2).**

*Source code of this project is available at [bitbucket](https://bitbucket.org/shaiju1983/cu_result_dna/src/86a1f9e8fc7d01e5a133efced7533d4592153b6f?at=master)*

Someone please refactor this code !!!
--------------------------------------

screenshots
-----------
![download.png](https://bitbucket.org/repo/76dzE8/images/3246004005-download.png)
![analyzer.png](https://bitbucket.org/repo/76dzE8/images/1293468247-analyzer.png)
![about.png](https://bitbucket.org/repo/76dzE8/images/1074176657-about.png)