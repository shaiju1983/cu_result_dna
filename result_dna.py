#result_dna.py
"""
 CU B.Tech Result Downloader and Analyzer
 Author:  Shaiju Paul 
          Thrissur
 Contact: paultsr@gmail.com
"""
import sys
import urllib2
import downloader
import sm_resultAnalysis
from colorama import Fore, Back, Style, init
init()
#import shutil
#from downloader import sems
#swname = 'Calicut University B.Tech Result Downloader and Analyzer'
#sems=downloader.sems
#print name
while True:
    print("\n\t\tMenu\n\t\t____\n\n\t(1)Download PDF\n\t(2)Analyze Result\n\t(a)bout\n\t(q)uit")
    choice = raw_input("\n\tEnter your choice >>> ").lower().rstrip()
    if choice=="q":
        break
    elif choice=='1':
        #execfile("downloader.py")
	try:
		downloader.download()
		print(Fore.RESET + Back.RESET + Style.RESET_ALL)
		#untrusted.execute()
	except urllib2.URLError as err: 
		print(Fore.RED + "\nCan't connect to the Server.Check the Internet Connection !")
		print(Fore.RESET + Back.RESET + Style.RESET_ALL)
	except : 
		print(Fore.RED + "\nInvalid Input.Enter the correct Value. ")
		print(Fore.RESET + Back.RESET + Style.RESET_ALL)
				
    elif choice=='2':
	#sys.argv=['sm_resultAnalysis.py','pdf/']
        #execfile("sm_resultAnalysis.py")
	sm_resultAnalysis.analyzer()
	print(Fore.RESET + Back.RESET + Style.RESET_ALL)
    elif choice=="a":
        print(Fore.BLUE + "#######################################################")
	print(Back.GREEN)
	print(Fore.RED + "B.Tech Result Analyzer scripted by Vinod K,SM RED Labs.\n\tContact: info@smredlabs.com ")
	print(Fore.RED + "B.Tech Result Downloader scripted at CSE Dept.,\n\tJyothi Engineering College, Thrissur.\n\tContact: cse@jecc.ac.in ")
	print(Back.RESET)
	print(Fore.BLUE + "#######################################################")
	print(Fore.RESET + Back.RESET + Style.RESET_ALL)
    else:
        print(Fore.RED + "Invalid choice, please choose again\n")
	print(Fore.RESET + Back.RESET + Style.RESET_ALL)
print(Back.GREEN)
print(Fore.RED + "\nThank you for using Calicut University B.Tech Result Downloader and Analyzer !!")
print(Fore.RESET + Back.RESET + Style.RESET_ALL)

